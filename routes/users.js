var express = require('express');
var router = express.Router();
const Sentry = require('@sentry/node');
Sentry.init({ dsn: 'https://cc0dff667bae4e60b794bb46fcbdb08c@sentry.io/1772449' });

/* GET users listing. */
router.get('/', function(req, res, next) {
  Sentry.captureException("This is a test, now checked in");
  res.send('respond with a resource');
});

module.exports = router;
